﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            char premiere_lettre;
            char derniere_lettre;
            string phrase;
            int longueur_chaine;

            Console.WriteLine("Saisissez une phrase : ");
            phrase = Console.ReadLine();
            premiere_lettre = phrase[0];
            longueur_chaine = phrase.Length;
            derniere_lettre = phrase[(longueur_chaine - 1)];
            Console.WriteLine("Vous avez saisie : ");
            Console.WriteLine(phrase);
            if(premiere_lettre >= 'A' && premiere_lettre <= 'Z')
            {
                Console.WriteLine("Cette phrase commence par une majuscule");
            }
            if(derniere_lettre == '.')
            {
                Console.WriteLine("Cette phrase se termine par un point");
            }


            Console.ReadKey();
        }
    }
}
