﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] TableaudeValeur = new int[10];
            bool ValeurPresenter = false;
            int IndexValeur, PositiondelaValeur=0, ValeurCherche;

            Console.WriteLine("Saisissez 10 valeurs");
            for (IndexValeur = 0; IndexValeur <= 9; IndexValeur++)
            {
                Console.WriteLine("{0} : ", (IndexValeur + 1));
                TableaudeValeur[IndexValeur] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Saisissez une valeur : ");
            ValeurCherche = int.Parse(Console.ReadLine());
            while (ValeurPresenter == false && IndexValeur <= TableaudeValeur.Length)
            {
                if(TableaudeValeur[IndexValeur] == ValeurCherche)
                {
                    ValeurPresenter = true;
                    PositiondelaValeur = IndexValeur + 1;
                }
            }
            if (ValeurPresenter == true)
            {
                Console.WriteLine("{0} se situe en {1} ème position", ValeurCherche, PositiondelaValeur);
            }

            Console.ReadKey();
        }
    }
}
